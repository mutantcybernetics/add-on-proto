# Add-on-Proto

# Proto_board is a dual-purpose prototyping shield with a stemmaQt connector.

## Compatible with mutantC v2/v3/v4

<img src="pic_main.png" width="500">
<img src="position.png" width="500">
<img src="show_off.png" width="500">
